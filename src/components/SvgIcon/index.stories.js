import SvgIcon from './index.vue';

export default {
  title: 'SvgIcon',
  component: SvgIcon
};

export const Default = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { SvgIcon },
  template: '<svg-icon name="user"/>',
});
