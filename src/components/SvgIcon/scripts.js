const icons = {
  home: () => import('./icons/HomeIcon.vue'),
  user: () => import('./icons/UserIcon.vue')
};

export default {
  name: 'svg-icon',
  props: {
    name: {
      type: String,
      required: true,
      validator(value) {
        return Object.prototype.hasOwnProperty.call(icons, value)
      }
    }
  },
  computed: {
    iconComponent() {
      return icons[this.name]
    }
  }
}
